//
//  chapterOne.h
//  Submarine
//
//  Created by Adam on 08/01/2014.
//  Copyright (c) 2014 Adam Ray Bicknell. All rights reserved.
//

#import <UIKit/UIKit.h>
int y;
int randomPosition;
BOOL begin;
BOOL surfaced;
int scoreNumber;
int highScore;
int timeElapsed;

@interface chapterOne : UIViewController
{
    IBOutlet UILabel *GameTitle;
    IBOutlet UILabel *TapToStart;
    IBOutlet UILabel *DevelopedBy;
    IBOutlet UILabel *lblHighScore;
    IBOutlet UILabel *GameResult;
    IBOutlet UIImageView *Submarine;
    
      
    NSTimer *timer;
    
    IBOutlet UIImageView *Rock;
    IBOutlet UIImageView *Rock2;
    IBOutlet UIImageView *SeaFloor1;
    IBOutlet UIImageView *SeaFloor2;
    IBOutlet UIImageView *SeaFloor3;
    IBOutlet UIImageView *SeaFloor4;
    IBOutlet UIImageView *SeaFloor5;
    IBOutlet UIImageView *SeaFloor6;
    IBOutlet UIImageView *SeaFloor7;
    IBOutlet UIImageView *Land1;
    IBOutlet UIImageView *Land2;
    IBOutlet UIImageView *Land3;
    IBOutlet UIImageView *Land4;
    IBOutlet UIImageView *Land5;
    IBOutlet UIImageView *Land6;
    IBOutlet UIImageView *Land7;
    IBOutlet UIButton *contSecond;
    
    IBOutlet UILabel *Score;
    NSTimer *timedScore;
    
    
    
    
}

-(void)SubmarineMove;
-(void)Crash;
-(void)NewGame;
-(void)EndGame;
-(void)timedScoring;
@end
