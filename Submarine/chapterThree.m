//
//  chapterThree.m
//  Submarine
//
//  Created by Adam on 28/01/2014.
//  Copyright (c) 2014 Adam Ray Bicknell. All rights reserved.
//

#import "chapterThree.h"

@interface chapterThree ()

@end

int y;
int randomPosition;
BOOL begin;
BOOL surfaced;
int scoreNumber;
int highScore;
int timeElapsed;
int starFishNumber;

int initialScore;
BOOL resetScoreIfReplay;

@implementation chapterThree

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)SubmarineMove{
    
    [self Crash];
    
    //Runs every 0.1 seconds
    Submarine.center = CGPointMake(Submarine.center.x, Submarine.center.y + y);
    
    //Makes submarine appear as if moving acrossing screen
    Rock.center = CGPointMake(Rock.center.x - 10, Rock.center.y);
    Rock2.center = CGPointMake(Rock2.center.x - 10, Rock2.center.y);
    smallRock.center = CGPointMake(smallRock.center.x - 10, smallRock.center.y);
    smallRock2.center = CGPointMake(smallRock2.center.x - 10, smallRock2.center.y);
    starFish.center = CGPointMake(starFish.center.x - 10, starFish.center.y);
    SeaFloor1.center = CGPointMake(SeaFloor1.center.x - 10, SeaFloor1.center.y);
    SeaFloor2.center = CGPointMake(SeaFloor2.center.x - 10, SeaFloor2.center.y);
    SeaFloor3.center = CGPointMake(SeaFloor3.center.x - 10, SeaFloor3.center.y);
    SeaFloor4.center = CGPointMake(SeaFloor4.center.x - 10, SeaFloor4.center.y);
    SeaFloor5.center = CGPointMake(SeaFloor5.center.x - 10, SeaFloor5.center.y);
    SeaFloor6.center = CGPointMake(SeaFloor6.center.x - 10, SeaFloor6.center.y);
    SeaFloor7.center = CGPointMake(SeaFloor7.center.x - 10, SeaFloor7.center.y);
    Land1.center = CGPointMake(Land1.center.x - 10, Land1.center.y);
    Land2.center = CGPointMake(Land2.center.x - 10, Land2.center.y);
    Land3.center = CGPointMake(Land3.center.x - 10, Land3.center.y);
    Land4.center = CGPointMake(Land4.center.x - 10, Land4.center.y);
    Land5.center = CGPointMake(Land5.center.x - 10, Land5.center.y);
    Land6.center = CGPointMake(Land6.center.x - 10, Land6.center.y);
    Land7.center = CGPointMake(Land7.center.x - 10, Land7.center.y);
    
    //Regenerate Rocks
    
    if (Rock.center.x < 0){
        randomPosition = arc4random() %75;
        randomPosition = randomPosition + 110;
        Rock.center = CGPointMake(510, randomPosition);
    }
    
    if (Rock2.center.x < 0){
        randomPosition = arc4random() %75;
        randomPosition = randomPosition + 110;
        Rock2.center = CGPointMake(510, randomPosition);
    }
    
    if (smallRock.center.x < 0){
        randomPosition = arc4random() %75;
        randomPosition = randomPosition + 110;
        smallRock.center = CGPointMake(510, randomPosition);
    }
    
    if (smallRock2.center.x < 0){
        randomPosition = arc4random() %75;
        randomPosition = randomPosition + 110;
        smallRock2.center = CGPointMake(510, randomPosition);
    }
    
    //Regenerate starFish if previous was collected (100pt each x 5. Possible 500 points.  If one is missed, no more regenerate.)
    
    if (starFish.center.x < 0 && starFish.hidden == YES && starFishNumber <= 5){
        scoreNumber = scoreNumber + 100;
        Score.text = [NSString stringWithFormat:@"Score: %i", scoreNumber];
        starFish.hidden = NO;
        starFishNumber = starFishNumber + 1;
        randomPosition = arc4random() %75;
        randomPosition = randomPosition + 110;
        starFish.center = CGPointMake(510, randomPosition);
    }
    
    //Regen Land and SeaFloor
    if (Land1.center.x < -30){
        randomPosition = arc4random() %55;
        Land1.center = CGPointMake(510, randomPosition);
        randomPosition = randomPosition + 265;
        SeaFloor1.center = CGPointMake(510, randomPosition);
    }
    if (Land2.center.x < -30){
        randomPosition = arc4random() %55;
        Land2.center = CGPointMake(510, randomPosition);
        randomPosition = randomPosition + 265;
        SeaFloor2.center = CGPointMake(510, randomPosition);
    }
    if (Land3.center.x < -30){
        randomPosition = arc4random() %55;
        Land3.center = CGPointMake(510, randomPosition);
        randomPosition = randomPosition + 265;
        SeaFloor3.center = CGPointMake(510, randomPosition);
    }
    if (Land4.center.x < -30){
        randomPosition = arc4random() %55;
        Land4.center = CGPointMake(510, randomPosition);
        randomPosition = randomPosition + 265;
        SeaFloor4.center = CGPointMake(510, randomPosition);
    }
    if (Land5.center.x < -30){
        randomPosition = arc4random() %55;
        Land5.center = CGPointMake(510, randomPosition);
        randomPosition = randomPosition + 265;
        SeaFloor5.center = CGPointMake(510, randomPosition);
    }
    if (Land6.center.x < -30){
        randomPosition = arc4random() %55;
        Land6.center = CGPointMake(510, randomPosition);
        randomPosition = randomPosition + 265;
        SeaFloor6.center = CGPointMake(510, randomPosition);
    }
    if (Land7.center.x < -30){
        randomPosition = arc4random() %55;
        Land7.center = CGPointMake(510, randomPosition);
        randomPosition = randomPosition + 265;
        SeaFloor7.center = CGPointMake(510, randomPosition);
    }
    
    
}

-(void)timedScoring {
    
    //Update score with 10 points every second
    scoreNumber = scoreNumber + 10;
    timeElapsed = timeElapsed + 1;
    Score.text = [NSString stringWithFormat:@"Score: %i", scoreNumber];
}

//users touches screen
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (timeElapsed < 45) {
        if(begin == YES)
        {
            //Hide start screen labels
            
            TapToStart.hidden = YES;
            
            timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(SubmarineMove) userInfo:nil repeats:YES];
            timedScore = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timedScoring) userInfo:nil repeats:YES];
            
            begin = NO;
            surfaced = NO;
            
            SeaFloor1.hidden = NO;
            SeaFloor2.hidden = NO;
            SeaFloor3.hidden = NO;
            SeaFloor4.hidden = NO;
            SeaFloor5.hidden = NO;
            SeaFloor6.hidden = NO;
            SeaFloor7.hidden = NO;
            Land1.hidden = NO;
            Land2.hidden = NO;
            Land3.hidden = NO;
            Land4.hidden = NO;
            Land5.hidden = NO;
            Land6.hidden = NO;
            Land7.hidden = NO;
            Rock.hidden = NO;
            Rock2.hidden = NO;
            starFish.hidden = NO;
            smallRock.hidden = NO;
            smallRock2.hidden = NO;
            Score.hidden = NO;
            lblHighScore.hidden = YES;
            GameResult.hidden = YES;
            
            //Random value between 0 and 74
            randomPosition = arc4random() %75;
            
            //Add 110 (All possible locations for Rock)
            randomPosition = randomPosition + 110;
            Rock.center = CGPointMake(570, randomPosition);
            
            
            //Random value between 0 and 74
            randomPosition = arc4random() %75;
            
            //Add 110 (All possible locations for Rock2)
            randomPosition = randomPosition + 110;
            Rock2.center = CGPointMake(855, randomPosition);
            
            
            
            
            //Random value between 0 and 74
            randomPosition = arc4random() %75;
            
            //Add 110 (All possible locations for smallRock)
            randomPosition = randomPosition + 110;
            smallRock.center = CGPointMake(1015, randomPosition);
            
            
            //Random value between 0 and 74
            randomPosition = arc4random() %75;
            
            //Add 110 (All possible locations for smallRock2)
            randomPosition = randomPosition + 110;
            smallRock2.center = CGPointMake(1285, randomPosition);
            
            
            
            //Random value between 0 and 74
            randomPosition = arc4random() %75;
            
            //Add 110 (All possible locations for starFish)
            randomPosition = randomPosition + 110;
            starFish.center = CGPointMake(730, randomPosition);
            
            
            
            
            
            
            
            
            
            randomPosition = arc4random() %55;
            Land1.center = CGPointMake(560, randomPosition);
            randomPosition = randomPosition + 265;
            SeaFloor1.center = CGPointMake(560, randomPosition);
            
            
            randomPosition = arc4random() %55;
            Land2.center = CGPointMake(640, randomPosition);
            randomPosition = randomPosition + 265;
            SeaFloor2.center = CGPointMake(640 , randomPosition);
            
            
            randomPosition = arc4random() %55;
            Land3.center = CGPointMake(720, randomPosition);
            randomPosition = randomPosition + 265;
            SeaFloor3.center = CGPointMake(720 , randomPosition);
            
            
            randomPosition = arc4random() %55;
            Land4.center = CGPointMake(800, randomPosition);
            randomPosition = randomPosition + 265;
            SeaFloor4.center = CGPointMake(800 , randomPosition);
            
            
            randomPosition = arc4random() %55;
            Land5.center = CGPointMake(880, randomPosition);
            randomPosition = randomPosition + 265;
            SeaFloor5.center = CGPointMake(880 , randomPosition);
            
            
            randomPosition = arc4random() %55;
            Land6.center = CGPointMake(960, randomPosition);
            randomPosition = randomPosition + 265;
            SeaFloor6.center = CGPointMake(960 , randomPosition);
            
            
            randomPosition = arc4random() %55;
            Land7.center = CGPointMake(1040, randomPosition);
            randomPosition = randomPosition + 265;
            SeaFloor7.center = CGPointMake(1040 , randomPosition);
            
            
            
            
        }
    }
    if (timeElapsed >= 45) {
        
        [timer invalidate];
        [timedScore invalidate];
        GameResult.text = [NSString stringWithFormat:@"Stage Complete!"];
        SeaFloor1.hidden = YES;
        SeaFloor2.hidden = YES;
        SeaFloor3.hidden = YES;
        SeaFloor4.hidden = YES;
        SeaFloor5.hidden = YES;
        SeaFloor6.hidden = YES;
        SeaFloor7.hidden = YES;
        Land1.hidden = YES;
        Land2.hidden = YES;
        Land3.hidden = YES;
        Land4.hidden = YES;
        Land5.hidden = YES;
        Land6.hidden = YES;
        Land7.hidden = YES;
        Rock.hidden = YES;
        Rock2.hidden = YES;
        starFish.hidden = YES;
        smallRock.hidden = YES;
        smallRock2.hidden = YES;
        contThird.hidden = NO;
        GameResult.hidden = NO;
        
        
        
    }
    
    y = -7;
    Submarine.image = [UIImage imageNamed:@"sub-up.png"];
    
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (surfaced != YES) {
        y = 7;
        Submarine.image = [UIImage imageNamed:@"sub-down.png"];
    }
    
    
}

- (void)viewDidLoad
{
    
    if (resetScoreIfReplay == TRUE)
    {
        scoreNumber = initialScore;
        
    }
    
    begin = YES;
    timeElapsed = 0;
    starFishNumber = 1;
    resetScoreIfReplay = FALSE;
    
    SeaFloor1.hidden = YES;
    SeaFloor2.hidden = YES;
    SeaFloor3.hidden = YES;
    SeaFloor4.hidden = YES;
    SeaFloor5.hidden = YES;
    SeaFloor6.hidden = YES;
    SeaFloor7.hidden = YES;
    Land1.hidden = YES;
    Land2.hidden = YES;
    Land3.hidden = YES;
    Land4.hidden = YES;
    Land5.hidden = YES;
    Land6.hidden = YES;
    Land7.hidden = YES;
    Rock.hidden = YES;
    Rock2.hidden = YES;
    smallRock.hidden = YES;
    smallRock2.hidden = YES;
    starFish.hidden = YES;
    Score.hidden = YES;
    contThird.hidden = YES;
    GameResult.hidden = YES;
    lblHighScore.hidden = YES;
    contThird.hidden = YES;
    startAgain.hidden = YES;
    
    highScore = [[NSUserDefaults standardUserDefaults] integerForKey:@"HighScoreSaved"];
    lblHighScore.text = [NSString stringWithFormat:@"High Score: %i", highScore];
    
    initialScore = scoreNumber;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)EndGame {
    
    //If current score is higher than high score set new high score
    if (scoreNumber > highScore) {
        highScore = scoreNumber;
        [[NSUserDefaults standardUserDefaults] setInteger:highScore forKey:@"HighScoreSaved"];
    }
    
    //Hit rock
    Submarine.hidden = YES;
    [timer invalidate];
    [timedScore invalidate];
    
    highScore = [[NSUserDefaults standardUserDefaults] integerForKey:@"HighScoreSaved"];
    lblHighScore.text = [NSString stringWithFormat:@"High Score: %i", highScore];
    
    
    if (surfaced == NO) {
        
        GameResult.text = [NSString stringWithFormat:@"You Crashed!"];
        
    }
    
    GameResult.hidden = NO;
    
    //Allow Restart
    startAgain.hidden = NO;
    
    resetScoreIfReplay = TRUE;
}

-(void) presentSecondViewControllerFromViewController:(UIViewController *)sourceController
{
    UIStoryboard* secondStoryboard = [UIStoryboard storyboardWithName:@"SecondStoryboard" bundle:nil];
    UIViewController* secondViewController = [secondStoryboard instantiateViewControllerWithIdentifier:@"second_viewcontroller"];
    
    [sourceController presentViewController: secondViewController animated:YES completion: NULL];
}

-(void)Crash {
    
    //Start Crashes//
    
    //If submarine hits Rock
    if (CGRectIntersectsRect(Submarine.frame, Rock.frame))
    {
        [self EndGame];
    }
    
    //If submarine hits Rock2
    if (CGRectIntersectsRect(Submarine.frame, Rock2.frame))
    {
        [self EndGame];
    }
    
    //If submarine hits smallRock
    if (CGRectIntersectsRect(Submarine.frame, smallRock.frame))
    {
        [self EndGame];
    }
    
    //If submarine hits smallRock2
    if (CGRectIntersectsRect(Submarine.frame, smallRock2.frame))
    {
        [self EndGame];
    }
    
    
    //If submarine hits SeaFloor1
    if (CGRectIntersectsRect(Submarine.frame, SeaFloor1.frame))
    {
        [self EndGame];
    }
    
    //If submarine hits SeaFloor2
    if (CGRectIntersectsRect(Submarine.frame, SeaFloor2.frame))
    {
        [self EndGame];
    }
    
    //If submarine hits SeaFloor3
    if (CGRectIntersectsRect(Submarine.frame, SeaFloor3.frame))
    {
        [self EndGame];
    }
    
    //If submarine hits SeaFloor4
    if (CGRectIntersectsRect(Submarine.frame, SeaFloor4.frame))
    {
        [self EndGame];
    }
    
    //If submarine hits SeaFloor5
    if (CGRectIntersectsRect(Submarine.frame, SeaFloor5.frame))
    {
        [self EndGame];
    }
    
    //If submarine hits SeaFloor6
    if (CGRectIntersectsRect(Submarine.frame, SeaFloor6.frame))
    {
        [self EndGame];
    }
    
    //If submarine hits SeaFloor7
    if (CGRectIntersectsRect(Submarine.frame, SeaFloor7.frame))
    {
        [self EndGame];
    }
    
    //If submarine hits Land1
    if (CGRectIntersectsRect(Submarine.frame, Land1.frame))
    {
        [self EndGame];
    }
    
    //If submarine hits Land2
    if (CGRectIntersectsRect(Submarine.frame, Land2.frame))
    {
        [self EndGame];
    }
    
    //If submarine hits Land3
    if (CGRectIntersectsRect(Submarine.frame, Land3.frame))
    {
        [self EndGame];
    }
    
    //If submarine hits Land4
    if (CGRectIntersectsRect(Submarine.frame, Land4.frame))
    {
        [self EndGame];
    }
    
    //If submarine hits Land5
    if (CGRectIntersectsRect(Submarine.frame, Land5.frame))
    {
        [self EndGame];
    }
    
    //If submarine hits Land6
    if (CGRectIntersectsRect(Submarine.frame, Land6.frame))
    {
        [self EndGame];
    }
    
    //If submarine hits Land7
    if (CGRectIntersectsRect(Submarine.frame, Land7.frame))
    {
        [self EndGame];
    }
    
    //Crash if submarine hits floor
    if (Submarine.center.y > 255) {
        
        [self EndGame];
        
    }
    //End Crashes//
    
    
    //Start Hit By Weapons//
    //If submarine surfaces before entering course it gets hit by missle
    if (Submarine.center.y < 35 && surfaced == NO) {
        
        Submarine.image = [UIImage imageNamed:@"hit-by-missle.png"];
        surfaced = YES;
    }
    
    if (surfaced == YES) {
        y = 0;
        GameResult.text = [NSString stringWithFormat:@"Don't surface! You're a sitting duck that way!"];
        GameResult.hidden = NO;
        [self performSelector:@selector(EndGame) withObject:nil afterDelay:1];
    }
    
    //End Hit By Weapons//
    
    
    //Start extra points//
    //starFish give 15 extra points
    if (CGRectIntersectsRect(Submarine.frame, starFish.frame)) {
        starFish.hidden = YES;
        
    }
    
    //End Extra Points//
    
}

@end
