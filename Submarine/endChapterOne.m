//
//  endChapterOne.m
//  Submarine
//
//  Created by Adam on 07/01/2014.
//  Copyright (c) 2014 Adam Ray Bicknell. All rights reserved.
//

#import "endChapterOne.h"

@interface endChapterOne ()

@end

@implementation endChapterOne

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [paragraphOne setAlpha:0];
    [paragraphTwo setAlpha:0];
    [paragraphThree setAlpha:0];
    [viewMapOne setAlpha:0];
    [self talkingFadeIn];
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)talkingFadeIn {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelay:0.0];
    [UIView setAnimationDuration:1.0];
    [paragraphOne setAlpha:0.5];
    [UIView setAnimationDelay:3.0];
    [UIView setAnimationDuration:1.0];
    [paragraphTwo setAlpha:0.5];
    [UIView setAnimationDelay:6.0];
    [UIView setAnimationDuration:1.0];
    [paragraphThree setAlpha:0.5];
    [UIView setAnimationDelay:6.0];
    [UIView setAnimationDuration:1.0];
    [viewMapOne setAlpha:1];
    [UIView commitAnimations];
    
}

@end
