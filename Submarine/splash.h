//
//  splash.h
//  Submarine
//
//  Created by Adam on 08/01/2014.
//  Copyright (c) 2014 Adam Ray Bicknell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface splash : UIViewController
{ 
    IBOutlet UIButton *startButton;
}

@property (nonatomic, strong) AVAudioPlayer *audioPlayer;

@end
