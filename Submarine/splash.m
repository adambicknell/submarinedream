//
//  splash.m
//  Submarine
//
//  Created by Adam on 08/01/2014.
//  Copyright (c) 2014 Adam Ray Bicknell. All rights reserved.
//

#import "splash.h"

@interface splash ()

@end

@implementation splash

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    //get audio url
    NSString *soundFileString = [[NSBundle mainBundle] pathForResource:@"BackgroundMusic" ofType:@"m4a"];
    NSURL *audioFile = [NSURL fileURLWithPath: soundFileString];
    
    //create audioplayer instance
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:audioFile error:nil];
    
    //loop
    self.audioPlayer.numberOfLoops = -1;
    
    //start
    [self.audioPlayer play];
    
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
