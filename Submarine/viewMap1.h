//
//  viewMap1.h
//  Submarine
//
//  Created by Adam on 07/01/2014.
//  Copyright (c) 2014 Adam Ray Bicknell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface viewMap1 : UIViewController
{
    
    IBOutlet UIButton *startChapter2;
    IBOutlet UIImageView *tapToEnter;
    IBOutlet UITextView *paragraphOne;
    IBOutlet UITextView *paragraphTwo;
    
}



@end
