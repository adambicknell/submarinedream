//
//  viewMapSix.h
//  Submarine
//
//  Created by Adam on 28/01/2014.
//  Copyright (c) 2014 Adam Ray Bicknell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface viewMapSix : UIViewController
{
    
IBOutlet UIButton *returnHome;
    IBOutlet UITextView *paragraphOne;
    IBOutlet UITextView *paragraphTwo;
    
}

@end
