//
//  viewMapThree.m
//  Submarine
//
//  Created by Adam on 28/01/2014.
//  Copyright (c) 2014 Adam Ray Bicknell. All rights reserved.
//

#import "viewMapThree.h"

@interface viewMapThree ()

@end

@implementation viewMapThree

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [paragraphOne setAlpha:0];
    [paragraphTwo setAlpha:0];
    [self FadeIn];
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)FadeIn {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelay:0.0];
    [UIView setAnimationDuration:1.0];
    [paragraphOne setAlpha:0.5];
    [UIView setAnimationDelay:5.0];
    [UIView setAnimationDuration:1.0];
    [paragraphTwo setAlpha:0.5];
    [UIView commitAnimations];
    
    [self performSelector:@selector(helpOn) withObject:self afterDelay:10.0];
    [self performSelector:@selector(helpOff) withObject:self afterDelay:15.0];
    [self performSelector:@selector(helpOn) withObject:self afterDelay:20.0];
    [self performSelector:@selector(helpOff) withObject:self afterDelay:25.0];
    
    
}


@end
