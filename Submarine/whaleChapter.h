//
//  whaleChapter.h
//  Submarine
//
//  Created by Adam on 28/01/2014.
//  Copyright (c) 2014 Adam Ray Bicknell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface whaleChapter : UIViewController
{
    IBOutlet UILabel *TapToStart;
    IBOutlet UILabel *GameResult;
    IBOutlet UIImageView *Submarine;
    
    NSTimer *timer;
    
   
    IBOutlet UIImageView *starFish;
    IBOutlet UIImageView *starFish2;
    IBOutlet UIImageView *starFish3;
    IBOutlet UIImageView *starFish4;
    IBOutlet UIImageView *starFish5;
    IBOutlet UIImageView *SeaFloor1;
    IBOutlet UIImageView *SeaFloor2;
    IBOutlet UIImageView *SeaFloor3;
    IBOutlet UIImageView *SeaFloor4;
    IBOutlet UIImageView *SeaFloor5;
    IBOutlet UIImageView *SeaFloor6;
    IBOutlet UIImageView *SeaFloor7;
    IBOutlet UIImageView *Land1;
    IBOutlet UIImageView *Land2;
    IBOutlet UIImageView *Land3;
    IBOutlet UIImageView *Land4;
    IBOutlet UIImageView *Land5;
    IBOutlet UIImageView *Land6;
    IBOutlet UIImageView *Land7;
    IBOutlet UIButton *contThird;
    IBOutlet UIButton *startAgain;
    IBOutlet UILabel *lblHighScore;
    
    IBOutlet UILabel *Score;
    NSTimer *timedScore;
    
    
    
    
}

-(void)SubmarineMove;
-(void)Crash;
-(void)EndGame;
-(void)timedScoring;
@end
