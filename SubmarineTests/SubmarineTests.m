//
//  SubmarineTests.m
//  SubmarineTests
//
//  Created by Adam on 04/01/2014.
//  Copyright (c) 2014 Adam Ray Bicknell. All rights reserved.
//

#import "SubmarineTests.h"

@implementation SubmarineTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in SubmarineTests");
}

@end
